package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {


//test
	@GetMapping("/")
	public String home() {
		return "Spring is here!";
	}
	// @GetMapping("/coucou")
    // public String coucou() {
    //     throw new RuntimeException("This endpoint is broken");
    // }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
